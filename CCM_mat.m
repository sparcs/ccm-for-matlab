%CCM_mat - Convergent Cross Mapping of Sugihara et al., 2012
%A MATLAB implementation of convergent cross mapping to detect causation
%in connected time series. Basic function. Like the C code of Hao Ye, this
%procedure checks all possible libraries and averages the results. Ties in 
%shortest distances are also taken into account.
%
%Citation:
%Sugihara, G., R. May, H. Ye, C. H. Hsieh, E. Deyle, M. Fogarty, and
%S. Munch. 2012. Detecting causality in complex ecosystems. Science 338:496-500.
%
%Usage:
%results = CCM_mat(X, Ydata, E, L, tau, b)  calculates the CCM of X xmap Y (=the effect
%of Y on X, each column of Ydata) for embedding dimension E, length of library L,
%time lag (tau) for the lagged-vector construction, and number of
%point for the simplex averaging b (default E+1)
%
%results.N = the length of the predicted time series
%results.L = the used library size(s)
%results.rho  = correlation coefficients between Y and Ypred
%results.mae = mean absolute errors
%results.rmse = root mean square errors
%
%Note that adding columns to Ydata takes little extra calculation time
%
%See also: causcorr
function results = CCM_mat(X, Ydata, E, L, tau, b)
    if nargin < 5
        tau = 1;
    end

    if nargin < 6
        b = E + 1;
    end

    if isa(X, 'table')
        X = table2array(X(:, 1));
    end

    if isa(Ydata, 'table')
        Ydata = table2array(Ydata);
    end
    
    if numel(L) > 1
        %if L is a vector, loop the elements
        r=zeros(numel(L), size(Ydata, 2));
        results = struct('N', 0, 'L', L, 'rho', r,  ...
            'mae', r, 'rmse', r);
        for i = 1:numel(L)
            res = CCM_mat(X, Ydata, E, L(i), tau, b);
            results.rho(i, :) = res.rho;
            results.mae(i, :) = res.mae;
            results.rmse(i, :) = res.rmse;
        end
        results.N = res.N;
        return;
    end   

    if L - E + 1 < b
        b = L - E + 1;
    end
    
    %generate time lags with length tau
    lagsX = zeros(length(X), E) + NaN;
    for i = 1:E
        ilag = (i - 1) * tau;
        lagsX(ilag + 1:length(X), i) = X(1:length(X) - ilag)';
    end
    lagsX = lagsX(1 + (E - 1) * tau:end, :);
    Ydata = Ydata(1 + (E - 1) * tau:end, :);
    N = size(lagsX, 1);
    % the library size cannot exceed N_data-(E - 1) * tau
    if L > N
        L = N;
    end
    
    if L == N
        %if the library size is equal to N, just select all data 1x
        Lstart = 1;
        Lndx = true(1, N);
    else
        %all possible libraries Note that it has periodic boundaries
        Lstart = 1:N;
        Lndx = false(size(Lstart, 2), N);
        for i = 1:length(Lstart)
            ndx1 = Lstart(i):Lstart(i) + L - 1;
            %periodic
            ndx1(ndx1 > N) = ndx1(ndx1 > N) - N;
            Lndx(i, ndx1) = true;
        end
    end
    nY = size(Ydata, 2);
    Ypred = zeros(N, length(Lstart), nY) + NaN;

    for i = 1:N
        Dtot = sqrt(sum((lagsX(i, :) - lagsX).^2, 2));
        % in old matlab the previous line does not work: 
        % Dtot = sqrt(sum(bsxfun(@minus, lagsX(i, :), lagsX).^2, 2));

        %leave one out crossvalidation
        Dtot(i) = NaN;
        for k = 1:length(Lstart)
            %select the library
            Lndxk = Lndx(k, :);
            D = Dtot(Lndxk, :);
            Yobs1 = Ydata(Lndxk, :);

            %find the b+1 closest points
            %mink is a fairly new & very fast command
            %find one more to test for ties at the end
            [Dmx, inm] = mink(D, b + 1, 'ComparisonMethod', 'real');
            %much slower alternative (old MATLAB versions):
            %D1 = D;
            %inm = zeros(1,min(b + 1, length(D)));
            %for m = 1:length(inm)
            %   [~, j] = min(D1);
            %   inm(m) = j;
            %   D1(j) = NaN;
            %end
            %Dmx = D(inm);
            has_ties = Dmx(b + 1) == Dmx(b);
            if has_ties
                %if the last distance equals the previous we have at least one
                %tie at the end. We add all ties in D to inm and Dmx
                tiedistance = Dmx(b);
                %find all ties in D and add them to inm and Dmx
                inm = [inm(Dmx ~= tiedistance); find(D == tiedistance)];
                Dmx= D(inm)';
            else
                %remove the extra slot for checking ties
                Dmx = Dmx(1:b)';
                inm = inm(1:b);
            end
            
            %weighted averaging of Y's of the closest points in embedded X
            if Dmx(1) > 0
                %inverse distance weighting (distance relative to the nearest)
                u = exp(-Dmx ./ Dmx(1));
                u(u < 0.000001) = 0.000001;
            else
                %perfect match at least once (we will get division by
                %zero for the inverse distance weighting).
                %We give an arbitrary small weight to all other and much stronger weight to
                %the match
                u = zeros(size(Dmx)) + 0.00001;
                u(Dmx == 0) = 1;
            end
            if has_ties
                %elements with ties
                tie_ndx = find(Dmx == tiedistance);
                %the adjustfactor weighs down the extra elements
                %It takes into account that we may have
                %had two ties at the end and then even more. 
                %examples: if b=4 and shortest distances are:
                % [a b c d] [e]     - no ties
                % [a a b c] [d]     - no ties
                % [a b c c] [d]     - no ties
                % [a b c d d] [e]   - adjust d's with 1/2
                % [a b c d d d] [e] - adjust d's with 1/3
                % [a b c c c] [d]   - adjust c's with 2/3
                adjustfactor = (b - tie_ndx(1) + 1) / numel(tie_ndx);
                u(tie_ndx) = u(tie_ndx) .* adjustfactor;
            end
            Ypred(i, k, :) = u * Yobs1(inm, :) ./ sum(u); %summing the weights
        end
    end
    %Some statistics
    rho = zeros(length(Lstart), nY) + NaN;
    mae = rho;
    rmse = rho;
    for k = 1:length(Lstart)
        for j = 1:nY
            Ypred1 = Ypred(:, k, j);
            Yobs1 = Ydata(~isnan(Ypred1), j);
            ndx = ~(isnan(Ypred1) | isnan(Yobs1));
            Ypred1 = Ypred1(ndx);
            Yobs2 = Yobs1(ndx);
            if ~isempty(Yobs1)
                rho(k, j) = pearson_r(Yobs2, Ypred1);
                mae(k, j) = mean(abs(Yobs2 - Ypred1));
                rmse(k, j) = sqrt(mean((Yobs2 - Ypred1).^2));
            end
        end
    end
    results = struct('N', N, 'L', L, 'rho', nanmean(rho), 'mae', nanmean(mae), 'rmse', nanmean(rmse));
end

function val = nanmean(x)
    val = zeros(1, size(x, 2));
    for i = 1:size(x, 2)
        val(i) = mean(x(~isnan(x(:, i)), i));
    end
end


function r = pearson_r(x, y)
    %fast and exactly the same as Hao's version
    %alternative: corr(x,y) gives the same result
    if ~isequal(size(x), size(y))
        r = NaN;
    else
        ndx = ~isnan(x) & ~isnan(y);
        x = x(ndx);
        y = y(ndx);
        n = numel(x);
        meanx = mean(x);
        varx = sum(x .* x) / n - meanx^2;
        meany = mean(y);
        vary = sum(y .* y) / n - meany^2;
        cov = sum(x .* y) / n - meanx * meany;

        r = cov / sqrt(varx * vary);
    end
end




