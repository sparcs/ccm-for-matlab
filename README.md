# CCM for MATLAB

This project is a MATLAB version of Convergent Cross Mapping (Sugihara et al. 2012) for detecting causality in complex systems.

## Requirements

Needed for running the code\
*MATLAB version R2020a or later is needed.\
*Parallel computing supported (replace parfor with for if needed)


## Description
This package has functions to run convergent cross mapping (Sugihara et al. 2012) in MATLAB. If the library size is smaller than the whole time series, all possible subsets are tested and the skill is averaged.
It also can run null models to test the significance of the results (Van Nes et al. 2015).

## References
Sugihara, G., R. May, H. Ye, C. H. Hsieh, E. Deyle, M. Fogarty, and S. Munch. 2012. Detecting causality in complex ecosystems. Science 338:496-500.\
van Nes, E. H., M. Scheffer, V. Brovkin, T. M. Lenton, H. Ye, E. Deyle, and G. Sugihara. 2015. Causal feedbacks in climate change. Nature Climate Change 5:445–448.


 
## Author
Egbert van Nes (egbert.vanNes@wur.nl)\
Wageningen University\
Aquatic Ecology and Water Quality Management group\
PO Box 47\
6700 AA Wageningen \
The Netherlands\

