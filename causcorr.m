%CAUSCORR - Convergent Cross Mapping of Sugihara et al., 2012
%A MATLAB implementation of convergent cross mapping to detect causation
%in connected time series. This function does the cross maps for a range of
%lengths of the library
%
%Citation:
%Sugihara, G., R. May, H. Ye, C. H. Hsieh, E. Deyle, M. Fogarty, and
%S. Munch. 2012. Detecting causality in complex ecosystems. Science 338:496-500.
%
%Usage:
%[corrs] = causcorr(X,Ydata, E, Ls, tau, b)  calculates cross maps of X on all columns 
%  of Ydata. (so the effect of each column of Y is evaluated on X.
%  Preferrable X and Ydata are datasets (see Statistics Toolbox). E =
%  embedding dimension, Ls is a vector with the lengths of the library,
%  b(optional) is the number of points used to map Y (by default b=E+1)
%
%
function [corrs1, names] = causcorr(X, Ydata, E, Ls, tau, b)
    if nargin < 5
        tau = 1;
    end
    if nargin < 6
        b = E + 1;
    end

    if isa(X, 'table')
        nameX = X.Properties.VariableNames;
        nameX = nameX{1};
        X = table2array(X(:, 1));
    else
        nameX = 'X';
    end
    if isa(Ydata, 'table')
        nameYs = Ydata.Properties.VariableNames;
        Y = table2array(Ydata);
    else
        Y = Ydata;
        nameYs = cell(1, size(Ydata, 2));
        for i = 1:size(Ydata, 2)
            nameYs{i} = sprintf('Y%d', i);
        end
    end
    %remove leading nans
    while ~isempty(X) && (isnan(X(1)) || isnan(Y(1)))
        X = X(2:end);
        Y = Y(2:end, :);
    end
    %remove trailing nans
    while ~isempty(X) && (isnan(X(end)) || isnan(Y(end)))
        X = X(1:end - 1);
        Y = Y(1:end - 1, :);
    end
    nY = size(Y, 2);

    if nargin < 4
        Ls = E:length(X);
    end

    Ls = Ls(Ls <= length(X));
    Ls = Ls(:);
    
    %native MATLAB implementation, can run in parallel
    res1 = CCM_mat(X, Y, E, Ls, tau, b);
    corrs = res1.rho;
    Ls = res1.L;
    
    names = cell(1, nY);
    for i = 1:nY
        names{i} = sprintf('%s_xmap_%s', nameX, nameYs{i});
    end
    % figure(1)
    % plot(1:length(X), [X, Y]);
    % legend([{nameX}; nameYs]);
    % figure(2);
    % plot(Ls, corrs)
    % legend(strrep(names,'_',' '));
    %make a table to preserve the var names
    %if ~isempty(ver('stats')) %statistics Toobox present?
    names = [{'L'}, names];
    %corrs = [corrs];
    %corrs = dataset([{[Ls', corrs]}, names]);
    corrs1 = array2table([Ls, corrs], 'VariableNames', names);
    %corrs1 = corrs;
end
