%CAUSCORR_NULL - Convergent Cross Mapping including null model
%Two null models supported (1) Swap the time series (2) Ebisuzaki
%(artificial data with same frequencies)
%
% MATLAB implementation of convergent cross mapping to detect causation
%in connected time series. This function does the cross maps for a range of
%lengths of the library
%
%Citation:
%Sugihara, G., R. May, H. Ye, C. H. Hsieh, E. Deyle, M. Fogarty, and
%S. Munch. 2012. Detecting causality in complex ecosystems. Science 338:496-500.
%
%Usage:
%[corrs,CL] = causcorr_null(X,Ydata, E, Ls, tau, b, method, nnull)  calculates cross maps of X on all columns
%  of Ydata. (so the effect of each column of Y is evaluated on X.
%  Preferrable X and Ydata are datasets (see Statistics Toolbox). E =
%  embedding dimension, Ls is a vector with the lengths of the library, tau
%  is a time lag,  b(optional) is the number of points used to map Y (by default b=E+1)
%  method ='swap' or 'ebi' (default swap) and nnull = number of null models (default 100)
%
% This is how you can plot the confidence limits
% figure
% h = fill([permcorr.L;flipud(permcorr.L)], [CL(:, 2, 1); flipud(CL(:, 1, 1))], 'b'); %Trick to fill the area
% set(h, 'facecolor', [0.8 0.8 1]);
%
function [corrsperm, CLs, perm] = causcorr_null(X, Ydata, E, Ls, tau, b, method, nnul)
    if nargin < 5
        tau = 1;
    end
    if nargin < 6
        b = E + 1;
    end
    if nargin < 7
        method = 'swap';
    end
    if nargin < 8
        nnul = 100;
    end
    if isa(X, 'table')
        f = X.Properties.VariableNames;
        name = f{1};
    else
        name = 'X';
    end
    if isa(Ydata, 'table')
        nameYs = Ydata.Properties.VariableNames;
    else
        nameYs = cell(1, size(Ydata, 2));
        for i = 1:size(Ydata, 2)
            nameYs{i} = sprintf('Y%d', i);
        end
    end
    alphas = [0.05 0.95];
    %We can only use simple cell arrays for in and output in parallel
    %computing and they need to be preloaded.
    %So we make the more complex variables outside of the parfor loop    
    perms = cell(nnul, 1);
    corrs = cell(nnul, 1);
    sievemodel = [];
    if strncmpi(method, 'swap', 2)
        method = 'swap';
    elseif strncmpi(method, 'sieve', 2)
        method = 'sieve';
        origdata = double(X);
        if isnan(origdata(1))
            [~, sievemodel] = sievebootstrap(origdata(2:end), nnul);
        elseif isnan(origdata(end))
            [~, sievemodel] = sievebootstrap(origdata(1:end - 1), nnul);
        else
            [~, sievemodel] = sievebootstrap(origdata, nnul);
        end
    elseif strncmpi(method, 'ebi', 1)
        method = 'ebi';
    else
        error('Method for null model ("%s") is unknown', method)
    end
   
    %to run sequential replace "parfor" with "for"
    %For debugging parfor should be replaced by for
    parfor i = 1:nnul
        name2 = sprintf('%s_%s%d', name, method, i);
        disp(name2);
        origdata = double(X);
        switch method
            case 'swap'
                if isnan(origdata(1))
                    perms{i} = [nan; swappermut(origdata(2:end))];
                else
                    perms{i} = swappermut(origdata);
                end
            case 'sieve'
                bootdata = sievebootstrap(sievemodel);
                bootdata = bootdata(:, i);
                if isnan(origdata(1))
                    perms{i} = [nan; bootdata];
                elseif isnan(origdata(end))
                    perms{i} = [bootdata; nan];
                else
                    perms{i} = bootdata;
                end
            case 'ebi'
                %the ebisuzaki method
                if isnan(origdata(1))
                    perms{i} = [nan; ebisuzaki(origdata(2:end), 1, - 1)];
                elseif isnan(origdata(end))
                    perms{i} = [ebisuzaki(origdata(1:end - 1), 1, - 1); nan];
                else
                    perms{i} = ebisuzaki(origdata, 1, -1);
                end
        end
        corrs{i} = causcorr(table(perms{i}, 'VariableNames', {name2}), Ydata, E, Ls, tau, b);
    end
    perm = table;
    names = cell(nnul, 1);
    for i = 1:nnul
        name2 = sprintf('%s_%s%d', name, method, i);
        perm.(name2) = perms{i};
        names{i} = name2;
        if i == 1
            corrsperm = corrs{i};
        else
            corrsperm = join(corrsperm, corrs{i});
        end
    end

    CLs = zeros(length(corrsperm.L), 2, length(nameYs));
    allfields = corrsperm.Properties.VariableNames;
    for i = 1:length(nameYs)
        %Temp_lin_ebi99_xmap_CH4
        fields = cell(1, nnul);
        for j = 1:nnul
            fields{j} = sprintf('%s_%s%d_xmap_%s', name, method, j, nameYs{i});
        end
        if all(ismember(fields, allfields))
            xx = corrsperm{:, fields};
            if size(xx, 1) == 1
                CL = quantile(double(xx)', alphas);
            else
                CL = quantile(double(xx)', alphas)';
            end
        else
            CL = zeros(size(alphas)) + NaN;
        end
        CLs(:, :, i) = CL;
    end
end


