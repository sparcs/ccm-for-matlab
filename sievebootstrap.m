
function [bootsets, res] = sievebootstrap(data, nboot)
    %sievebootstrap generate bootstrapped data sets.
    %using the sieve bootstrap (Bühlmann, 1997). First an AR(n) model is fitted
    %and the residuals determined. By sampling from the scaled residuals we
    %generate nboot new data series using the fitted AR model.
    %
    %Usage
    %sets=sievebootstrap(data) the bootstrapped data sets are generated,
    %    data should be a vector, the default nboot(=1000)
    %    res is a matrix with the same number of rows as the data. The
    %    columns are the different bootstrapped sets.
    %[sets,res]=sievebootstrap(data,nboot) with specified number of boot samples
    %    the structure res contains all information to recreate the bootstrapped data
    %    sets but saves a lot of disc space.
    %sets=sievebootstrap(res) - res is the saved structure. sets is recreated with the same random seed 
    %as before.
    %
    %reference: Bühlmann, P. (1997). Sieve bootstrap for time series. Bernoulli, 123-148.

    %res saves the random seed and used model and data so that it can remake exactly the
    %same bootstrapped data sets 
    if nargin < 2
        nboot = 1000;
    end
    %sieve bootstrap
    %(1) determine a full AR model (should test here for higher orders)
    %we take the model with the lowest AIC between lag order 1 and 10
    if ~isstruct(data)
        res.nboot = nboot;
        res.data = data;
        meandata = mean(data);
        data = data - meandata;
        tbl = lagtable(data, 10);
        ndx = true(size(tbl, 2), 1);
        aic = zeros(size(tbl, 2) - 1, 1);
        for order = length(aic): -1:1
            m = fitlm(tbl(:, ndx), 'intercept', false);
            aic(order) = m.ModelCriterion.AIC;
            ndx(order) = false;
        end
        order = find(min(aic) == aic);
        ndx = true(size(tbl, 2), 1);
        ndx(order + 1:end - 1) = false;

        model = fitlm(tbl(:, ndx), 'intercept', false);
        res.ARcoefs = fliplr(model.Coefficients.Estimate');
        res.seed = rng;
    else
        if isfield(data,'bootstrap')
            res=data.bootstrap;
        else
            res = data;
        end
        nboot=res.nboot;
        rng(res.seed);
        meandata = mean(res.data);
    end
    %get the AR coefficients, fliplr because we need the highest order first
    order = numel(res.ARcoefs);
    data = res.data - meandata;
    ndata = numel(res.data);
    %(2)determine the residuals
    residuals = nan(size(data));
    for i = order + 1:ndata
        %matrix multiplication with the ARcoefficients - data
        residuals(i) = res.ARcoefs * data(i - order:i - 1, :) - data(i);
    end
    %residuals = res.model.Residuals.Raw;
    %remove nan's at the end
    residuals = residuals(~isnan(residuals));

    %residuals=data(2:end)-rc*data(1:end-1);
    %(3)scale the errors to have a mean of zero
    residuals =  mean(residuals)-residuals'; %scale the errors
    n = numel(residuals);
    %(4)start with the first data point in the time series
    %run AR1 model for the size of the original data +1000 steps for stabilizing
    %(vectorized 1000 replicates) by sampling from the residuals with replacement
    bootsets = zeros(ndata + 1000, nboot);
    bootsets(1:order, :) = zeros(order, nboot) + data(1);
    for i = order + 1:ndata + 1000
        %stabilize + run the AR1 model
        %matrix multiplication with the ARcoefficients, + draw independent residuals
        bootsets(i, :) = res.ARcoefs * bootsets(i - order:i - 1, :) + residuals(randi(n, 1, nboot));
    end
    %(5)remove stabilizing part from the result
    bootsets = bootsets(1001:end, :);
end

function tbl = lagtable(data, maxlags)
    res1 = cell(maxlags, 1);
    for lag = 1:maxlags
        res1{lag} = nan(size(data));
        res1{lag}(1 + lag:end) = data(1:end - lag);
    end
    tbl = table(res1{:}, data);
end
