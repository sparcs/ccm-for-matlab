function res = swappermut(origdata)
    n = length(origdata);
    swappoint = floor(rand(1) * n) + 1; %random value between 1 and n
    if swappoint > 1
        %swap data
        res = [origdata(swappoint:n); origdata(1:swappoint - 1)];
    else
        %if swappoint=1 use original data
        res = origdata;
    end
end
