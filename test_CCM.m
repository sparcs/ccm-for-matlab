load sardine_anchov
%making a lot of ties
sard.SIO_SST(2:20) = mean(sard.SIO_SST(2:20));
disp('running CCM_mat with ties');
tic;
r1 = causcorr(sard.SIO_SST, sard.SARDINE, 3, [20:10:78]', 1, 4)
toc;

try
    %this test is running against the C++ code of Hao Ye. This is only done
    %if the C code is available. (It is not provided in the gitlab)
    disp('trying to run CCM_hao');
    tic
    r2 = causcorr_hao(sard.SIO_SST, sard.SARDINE, 3, [20:10:78], 1, 4)
    hashao = true;
catch
    disp('CCM_hao is not in the search path');
    hashao = false;
end

if hashao
    n = numel(r2.L);
    if max(abs(r1.X_xmap_Y1(1:n) - r2.X_xmap_Y1(1:n))) > 1E-6
        error('CCM_hao and CCM_mat are not the same');
    else
        disp('the results are the same');
    end
end

load sardine_anchov;
disp('running CCM_mat');
tic;
r1 = causcorr(sard.SIO_SST, sard.SARDINE, 3, [20:78]', 1, 4)
toc;
if hashao
    disp('running CCM_hao');
    tic;
    r2 = causcorr_hao(sard.SIO_SST, sard.SARDINE, 3, [20:78], 1, 4)
    toc;
    n = numel(r2.L);
    if max(abs(r1.X_xmap_Y1(1:n) - r2.X_xmap_Y1(1:n))) > 1E-6
        error('CCM_hao and CCM_mat are not the same');
    else
        disp('the results are the same');
    end
end

%tic;r2=causcorr1(sard.SIO_SST,sard.SARDINE,3,[20:20:78]',1,4),toc;
tic;
r3 = causcorr(sard.SIO_SST, sard(:, 3:end), 3, [20:20:78]', 1, 4)
toc;
assert(r3{1, 1} == r1{1, 1}, 'not the same')
tic;
r4 = causcorr(sard.SIO_SST, sard(:, 3:end), 3, [20:20:78], 1, 4)
toc;

if max(abs(r3.X_xmap_SIO_SST - r4.X_xmap_SIO_SST)) > 1E-6
    error('CCM_hao and CCM_mat are not the same');
else
    disp('the results are the same');
end
tic;
r3 = causcorr(sard.SIO_SST, sard(:, 3:end), 3, [20:20:78]', 2, 4)
toc;
if hashao
    tic;
    r4 = causcorr_hao(sard.SIO_SST, sard(:, 3:end), 3, [20:20:78], 2, 4)
    toc;
    if max(abs(r3.X_xmap_SIO_SST - r4.X_xmap_SIO_SST)) > 1E-6
        error('CCM_hao and CCM_mat are not the same');
    else
        disp('the results are the same');
    end
end

tic;
[r3, CLs] = causcorr_null(sard.SIO_SST, sard.SARDINE, 3, [20:20:78]', 2, 4, 'e', 100)
toc;
assert(any(strncmp('X_ebi1_xmap', r3.Properties.VariableNames, 11)), 'Wrong method');

tic;
[r3, CLs] = causcorr_null(sard.SIO_SST, sard(:, 3:end), 3, [20:20:78]', 1, 4, 'si', 100)
toc;
assert(any(strncmp('X_sieve1_xmap', r3.Properties.VariableNames, 13)), 'Wrong method');

